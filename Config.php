<?php

namespace xtetis\xforum;

class Config extends \xtetis\xengine\models\Model
{
    /**
     * Проверяет параметры
     */
    public static function validateParams()
    {
        
        if (!defined('XFORUM_PARENT_GALLERY_CATEGORY'))
        {
            throw new \Exception('Не определена константа XFORUM_PARENT_GALLERY_CATEGORY');
        }

        if (!is_integer(XFORUM_PARENT_GALLERY_CATEGORY))
        {
            throw new \Exception('Константа XFORUM_PARENT_GALLERY_CATEGORY должна быть сущесвующей категорией');
        }

        if (!XFORUM_PARENT_GALLERY_CATEGORY)
        {
            throw new \Exception('Константа XFORUM_PARENT_GALLERY_CATEGORY должна быть сущесвующей категорией');
        }

        $model_gallery_category = \xtetis\ximg\models\CategoryModel::generateModelById(XFORUM_PARENT_GALLERY_CATEGORY);
        
        if (!$model_gallery_category)
        {
            throw new \Exception('Константа XFORUM_PARENT_GALLERY_CATEGORY должна быть сущесвующей категорией');
        }
        
    }

}
