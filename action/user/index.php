<?php

/**
 * Профиль пользователя форума
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

$id   = \xtetis\xengine\helpers\RequestHelper::get('id', 'int', 0);
$page = \xtetis\xengine\helpers\RequestHelper::get('p', 'int', 1);


\xtetis\xengine\App::getApp()->setParam('layout', 'list');


$model_forum_user = new \xtetis\xforum\models\ForumUserModel([
    'offset' => (($page - 1) * 20),
]);
$model_forum_user->getForumUserModelListParams();

if ($model_forum_user->getErrors())
{
    throw new \Exception($model_forum_user->getLastErrorMessage());
}

// Урлы
// ------------------------------------------------
$urls['url_forum']         = self::makeUrl();
$url_current_page_paginate = $urls['url_forum_users'] = self::makeUrl([
    'query' => [
        'user',
    ],
]);
// ------------------------------------------------

$pagination = \xtetis\xengine\helpers\PaginateHelper::getpagination(
    20,
    $page,
    $model_forum_user->total_count,
    $url_current_page_paginate
);

// Рендерим текущую страницу
echo \xtetis\xengine\App::getApp()->renderCurrentPage(
    [
        'urls'             => $urls,
        'model_forum_user' => $model_forum_user,
        'pagination'       => $pagination,
    ],
);
