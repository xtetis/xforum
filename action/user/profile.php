<?php

/**
 * Профиль пользователя форума
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

$id = \xtetis\xengine\helpers\RequestHelper::get('id', 'int', 0);
$id = \xtetis\xengine\helpers\RequestHelper::get('id', 'int', 0);

$model = new \xtetis\xforum\models\ForumUserModel([
    'id_user' => $id,
]);

$model = $model->getForumUserModel();

if (!$model)
{
    http_response_code(404);
    \xtetis\xengine\helpers\LogHelper::customDie('Пользователь не найден');
}

// Урлы
// ------------------------------------------------
$urls['url_forum'] = self::makeUrl();

$urls['url_forum_users'] = self::makeUrl([
    'path' => [
        'user',
    ],
]);

$urls['url_forum_user_themes'] = self::makeUrl([
    'path' => [
        'user',
        $id,
    ],
]);

$urls['url_forum_user_messages'] = self::makeUrl([
    'path' => [
        'user',
        $id,
    ],
    'query'=>[
        'info'=>'messages'
    ]
]);


// ------------------------------------------------

// Рендерим текущую страницу
echo \xtetis\xengine\App::getApp()->renderCurrentPage(
    [
        'urls'  => $urls,
        'model' => $model,
    ],
);
