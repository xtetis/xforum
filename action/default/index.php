<?php

/**
 * Рендер списка групп
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}
\xtetis\xengine\App::getApp()->setParam('layout', 'list');

$model_group_list = \xtetis\xforum\models\GroupModel::getAllGroupModels();

// Урлы
// ------------------------------------------------
$urls['url_forum'] = self::makeUrl();

// ------------------------------------------------

// Рендерим текущую страницу
echo \xtetis\xengine\App::getApp()->renderCurrentPage(
    [
        'model_group_list' => $model_group_list,
        'urls'             => $urls,
    ],
);
