<?php

/**
 * Просмотр темы
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

\xtetis\xengine\App::getApp()->setParam('layout', 'list');

$id = \xtetis\xengine\helpers\RequestHelper::get('id', 'int', 0);
$page = \xtetis\xengine\helpers\RequestHelper::get('p', 'int', 0);
if ($page < 1)
{
    $page = 1;
}



$model_theme = \xtetis\xforum\models\ThemeModel::generateModelById($id);

if (!$model_theme)
{
    \xtetis\xengine\helpers\LogHelper::customDie('Тема не найдена');
}

$model_group = \xtetis\xforum\models\GroupModel::generateModelById($model_theme->id_group);
if (!$model_group)
{
    http_response_code(404);
    \xtetis\xengine\helpers\LogHelper::customDie('Группа не найдена');
}

if (!intval($model_theme->published))
{
    http_response_code(404);
    \xtetis\xengine\helpers\LogHelper::customDie('Тема еще не проверена модератором и не опубликована');
}

if (intval($model_theme->deleted))
{
    http_response_code(404);
    \xtetis\xengine\helpers\LogHelper::customDie('Тема удалена');
}

$parent_group_model = $model_group->getParentGroupModel();

// Урлы
// ------------------------------------------------
$urls['url_forum']                = self::makeUrl();
$urls['url_validate_add_message'] = self::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'ajax_validate_add_message',
    ],
]);
// ------------------------------------------------

// Список сообщений
$model_messages_list_info = $model_theme->getMessageModelPageListInfo($page);






// HTML код пагинации
$pagination = \xtetis\xengine\helpers\PaginateHelper::getPagination(
    20,
    $page,
    $model_messages_list_info['count'],
    // Текущая страница для генерации пагинации
    $model_theme->getLink()
);

// Рендерим текущую страницу
echo \xtetis\xengine\App::getApp()->renderCurrentPage(
    [
        'urls'                     => $urls,
        'model_theme'              => $model_theme,
        'model_group'              => $model_group,
        'parent_group_model'       => $parent_group_model,
        'pagination'               => $pagination,
        'model_messages_list_info' => $model_messages_list_info,
    ],
);
