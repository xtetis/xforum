<?php

/**
 * Валидация при добавлении темы
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

$response = [
    'result' => false,
];
$response['js_success'] = 'xform.goToUrl();';
$response['js_fail']    = '';

$id_theme   = \xtetis\xengine\helpers\RequestHelper::post('id_theme', 'int', 0);
$about      = \xtetis\xengine\helpers\RequestHelper::post('about', 'str', '');
$image_list = $_POST['image_list'] ?? [];

$model = new \xtetis\xforum\models\MessageModel(
    [
        'id_theme'   => $id_theme,
        'about'      => $about,
        'image_list' => $image_list,
    ]
);

if ($model->addMessage())
{
    $response['result']            = true;
    $response['data']['go_to_url'] = $model->getLink();
}
else
{
    $response['errors'] = $model->getErrors();
}

echo \xtetis\xengine\helpers\JsonHelper::arrayToJson($response);
exit;
