<?php

/**
 * Просмотр темы
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}



$id = \xtetis\xengine\helpers\RequestHelper::get('id', 'int', 0);


$model = \xtetis\xforum\models\ThemeModel::generateModelById($id);

if (!$model)
{
    \xtetis\xengine\helpers\LogHelper::customDie('Тема не найдена');
}

header("HTTP/1.1 301 Moved Permanently");
header("Location: ".$model->getLink());
exit();