<?php

/**
 * Валидация при добавлении темы
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

$response = [
    'result' => false,
];
$response['js_success'] = 'xform.goToUrl();';
$response['js_fail']    = '';

$name       = \xtetis\xengine\helpers\RequestHelper::post('name', 'str', '');
$id_group   = \xtetis\xengine\helpers\RequestHelper::post('id_group', 'int', '');
$about      = \xtetis\xengine\helpers\RequestHelper::post('about', 'str', '');
$image_list = $_POST['image_list'] ?? [];

$model = new \xtetis\xforum\models\ThemeModel(
    [
        'name'       => $name,
        'id_group'   => $id_group,
        'about'      => $about,
        'image_list' => $image_list,
    ]
);

if ($model->addTheme())
{

    $go_to_url = \xtetis\xforum\Component::makeUrl([
        'path' => [
            \xtetis\xengine\App::getApp()->getAction(),
            'created',
        ],
    ]);

    $response['result']            = true;
    $response['data']['go_to_url'] = $go_to_url;
}
else
{
    $response['errors'] = $model->getErrors();
}

echo \xtetis\xengine\helpers\JsonHelper::arrayToJson($response);
exit;
