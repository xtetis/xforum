<?php

/**
 * Добавление темы форума
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

$id_group = \xtetis\xengine\helpers\RequestHelper::get('id_group', 'int', 0);

// Если пользователь не авторизирован - отправляем его на форму авторизации
if (!\xtetis\xuser\Component::isLoggedIn())
{
    $url_login = \xtetis\xuser\Component::makeUrl([
        'path' => [
            'login',
        ],
    ]);

    header('Location: ' . $url_login);
    exit;

}

// Урлы
// ------------------------------------------------
$urls['url_forum']              = self::makeUrl();
$urls['url_validate_add_theme'] = self::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'ajax_validate_add_theme',
    ],
]);
// ------------------------------------------------

// Рендерим текущую страницу
echo \xtetis\xengine\App::getApp()->renderCurrentPage(
    [
        'urls'     => $urls,
        'id_group' => $id_group,
    ],
);
