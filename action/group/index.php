<?php

/**
 * Рендер списка групп
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}
\xtetis\xengine\App::getApp()->setParam('layout', 'list');

$id   = \xtetis\xengine\helpers\RequestHelper::get('id', 'int', 0);
$page = \xtetis\xengine\helpers\RequestHelper::get('p', 'int', 0);

//print_r($_SERVER);


if ($page < 1)
{
    $page = 1;
}

$model_group = \xtetis\xforum\models\GroupModel::generateModelById($id);

if (!$model_group)
{
    \xtetis\xengine\helpers\LogHelper::customDie('Группа не найдена');
}



if  (
        ($_SERVER['SCRIPT_URL']=='/forum/group') &&
        ($page == 1)
)
{
    header("HTTP/1.1 301 Moved Permanently");  
    header("Location: ".$model_group->getLink());  
    header("Connection: close"); 
    exit;
}

$parent_group_model = $model_group->getParentGroupModel();

// Список тем
$model_themes_list_info = $model_group->getGroupThemesModelPageListInfo([
    'offset'=>($page-1)*20
]); 



// HTML код пагинации
$pagination = \xtetis\xengine\helpers\PaginateHelper::getPagination(
    20,
    $page,
    $model_themes_list_info['count'],
    // Текущая страница для генерации пагинации
    $model_group->getLink()
);

// Урлы
// ------------------------------------------------
$urls['url_forum'] = self::makeUrl();

// ------------------------------------------------

// Рендерим текущую страницу
echo \xtetis\xengine\App::getApp()->renderCurrentPage(
    [
        'model_group'            => $model_group,
        'parent_group_model'     => $parent_group_model,
        'model_themes_list_info' => $model_themes_list_info,
        'urls'                   => $urls,
        'pagination'             => $pagination,
    ],
);
