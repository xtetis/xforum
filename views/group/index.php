<?php

    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }

    $h1 = $model_group->name;


    // Если есть родительская группа
    if ($parent_group_model)
    {
        \xtetis\xengine\App::getApp()->setParam('breadcrumbs', [
            [
                'name' => 'Форум',
                'url'  => $urls['url_forum'],
            ],
            [
                'name' => $parent_group_model->name,
                'url'  => $parent_group_model->getLink(),
            ],
            [
                'name' => $h1,
            ]
        ]);

        // Устанавливаем Title страницы
        \xtetis\xengine\helpers\SeoHelper::setTitle(
            'Форум - ' . 
            $parent_group_model->name . 
            ' - '. 
            $h1 . 
            ' - '. 
            APP_NAME
        );

        // Устанавливаем Description страницы
        \xtetis\xengine\helpers\SeoHelper::setDescription(
            $model_group->about.
            ': '.
            $model_group->name.' - '.
            $parent_group_model->name.
            '.'
        );
    }
    else
    {
        \xtetis\xengine\App::getApp()->setParam('breadcrumbs', [
            [
                'name' => 'Форум',
                'url'  => $urls['url_forum'],
            ],
            [
                'name' => $h1,
            ]
        ]);

        // Устанавливаем Title страницы
        \xtetis\xengine\helpers\SeoHelper::setTitle(
            'Форум - ' . 
            $h1. 
            ' - '. 
            APP_NAME
        );

        // Устанавливаем Description страницы
        \xtetis\xengine\helpers\SeoHelper::setDescription(
            $model_group->about.': '.$model_group->name.'.'
        );

    }


    // Устанавливаем H1 страницы
    \xtetis\xengine\helpers\SeoHelper::setPageName(
        $h1
    );

?>
<style>
.card-xform {}

.card-xform a.nav-link.active {
    color: #000 !important;
}

.card-xform a.nav-link {
    color: #fff;
}

</style>

<div class="card card-xform">
    <div class="card-header text-white bg-secondary ">

        <h1 class=" text-white">
            <?=$h1?>
        </h1>
    

        <div style="padding-bottom: 10px;">
            <?=$model_group->about?>
        </div>

        <ul class="nav nav-tabs card-header-tabs">
            <li class="nav-item">
                <a class="nav-link active "
                   id="last-themes-tab-<?=$model_group->id?>"
                   data-toggle="tab"
                   href="#last-themes-<?=$model_group->id?>"
                   role="tab"
                   aria-controls="last-themes-<?=$model_group->id?>"
                   aria-selected="true">Темы</a>
            </li>
            <?php if ($model_group->getChildGroupModelList()):?>
            <li class="nav-item">
                <a class="nav-link  text-white"
                   id="home-tab-<?=$model_group->id?>"
                   data-toggle="tab"
                   href="#home-<?=$model_group->id?>"
                   role="tab"
                   aria-controls="home-<?=$model_group->id?>"
                   aria-selected="false">Дочерние группы</a>
            </li>
            <?php endif; ?>
            <li class="nav-item">
                <a class="nav-link"
                   href="<?=$model_group->getLinkAddTheme()?>">
                   Добавить тему
                   <i class="fa fa-plus" aria-hidden="true"></i>


                </a>
            </li>
            
        </ul>
    </div>
    <div class="card-body">
        <div class="tab-content"
             id="myTabContent">

            <div class="tab-pane fade show active"
                 id="last-themes-<?=$model_group->id?>"
                 role="tabpanel"
                 aria-labelledby="last-themes-tab-<?=$model_group->id?>">
                 <?=\xtetis\xforum\Component::renderBlock(
                    'blocks/themes_list',
                    [
                        'model_list'=>$model_themes_list_info['models']
                    ]
                )?>
                <div>
                    <?=$pagination?>
                </div>
            </div>
            <?php if ($model_group->getChildGroupModelList()):?>
            <div class="tab-pane fade"
                 id="home-<?=$model_group->id?>"
                 role="tabpanel"
                 aria-labelledby="home-tab-<?=$model_group->id?>">
                <ul class="list-group list-group-flush">
                <?php foreach($model_group->getChildGroupModelList() as $id_child => $model_group_child):?>
                    <li class="list-group-item">
                        <a href="<?=$model_group_child->getLink()?>">
                            <?=$model_group_child->name?>
                        </a>
                    </li>
                <?php endforeach;?>
                </ul>
            </div>
            <?php endif; ?>
        </div>
    </div>

</div>



