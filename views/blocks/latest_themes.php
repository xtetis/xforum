<?php

    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }

    echo \xtetis\xforum\Component::renderBlock(
        'blocks/themes_list',
        [
            'model_list'=>$model_themes_list_info['models']
        ]
    )
?>
