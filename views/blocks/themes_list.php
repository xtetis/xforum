<?php

    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }
    

?>



<?php foreach ($model_list as $id_theme => $model_theme):?>
<?php
$model_user_author = $model_theme->getModelRelated('id_user');    
?>
<div class="row"
     style="border-bottom: 2px gray solid; margin-bottom: 10px;">
    <div class="col-12 col-sm-9">
        <div style="float: left; padding: 2px; padding-right: 6px;">
            <img src="<?=$model_user_author->getGravatarImgSrc()?>" alt="" srcset="" style="    max-height: 40px;">
        </div>
        <div>
            <a href="<?=$model_theme->getLink()?>"
               style="font-weight: bold; font-size: 1.2em;">
                <?=$model_theme->name?>
            </a>
        </div>
        <div style="margin-top: 5px; margin-bottom: 5px;">
            Создана:
            <?=$model_user_author->getUserLoginOrName()?>,
            <?=$model_theme->create_date?>
        </div>

    </div>
    <div class="col-12 col-sm-3">
        Ответов:
        <?=$model_theme->getMessageCount()?>
    </div>
</div>
<?php endforeach;?>
