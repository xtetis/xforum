<?php

    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }

    \xtetis\xengine\App::getApp()->setParam('breadcrumbs', [
        [
            'name' => 'Форум',
            'url'  => $urls['url_forum'],
        ],
        [
            'name' => 'Тема добавлена',
        ],
    ]);

    // Устанавливаем Title страницы
    \xtetis\xengine\helpers\SeoHelper::setTitle('Форум - Тема создана - ' . APP_NAME);
?>


<h3>
Тема добавлена
</h3>

<br>
<p>
    Тема на форуме создана. После проверки модератором - она будет опубликована.
</p>
<p>
    Пока Вы можете ознакомиться с другими
    <a href="<?=$urls['url_forum']?>"> темами форума</a>
</p>