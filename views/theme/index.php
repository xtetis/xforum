<?php

    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }

    if ($parent_group_model)
    {
        \xtetis\xengine\App::getApp()->setParam('breadcrumbs', [
            [
                'name' => 'Форум',
                'url'  => $urls['url_forum'],
            ],
            [
                'name' => $parent_group_model->name,
                'url'  => $parent_group_model->getLink(),
            ],
            [
                'name' => $model_group->name,
                'url'  => $model_group->getLink(),
            ],
            [
                'name' => $model_theme->name,
            ],
        ]);
    }
    else
    {
        \xtetis\xengine\App::getApp()->setParam('breadcrumbs', [
            [
                'name' => 'Форум',
                'url'  => $urls['url_forum'],
            ],
            [
                'name' => $model_group->name,
                'url'  => $model_group->getLink(),
            ],
            [
                'name' => $model_theme->name,
            ],
        ]);
    }

    $re = '/[!.\(\)=]/imXs';
    $title_meta = preg_replace($re,'',$model_theme->name);
    $title_meta = str_replace('  ',' ',$title_meta);

    // Устанавливаем Title страницы
    \xtetis\xengine\helpers\SeoHelper::setTitle(
        $title_meta . 
        ' - форум ' . 
        $model_group->name . ' - ' . APP_NAME);

    $about_descr = strip_tags($model_theme->about);

    $re = '/[!.\(\)=]/imXs';
    $about_descr = preg_replace($re,'',$about_descr);
    $about_descr = str_replace('  ',' ',$about_descr);




    // Устанавливаем Description страницы
    \xtetis\xengine\helpers\SeoHelper::setDescription(
        $title_meta.
        ': '.
        mb_substr( $about_descr,0,300)
    );
?>

<div class="card">
    <div class="card-header bg-secondary ">
        <h1 class=" text-white" style=" font-size: 1.25rem; margin: 0;">
            <?=$model_theme->name?>
        </h1>
    </div>
    <div class="card-body border-right border-bottom border-left border-primary">
        <div class="row">
            <div class="col-12 col-sm-3">
                <?=$model_theme->create_date?>    
                <br>
                <?=$model_theme->getModelRelated('id_user')->name?>
                <br>
                <br>
                <?=\xtetis\xmessage\Component::getButtonSendMessage($model_theme->id_user)?>
            </div>
            <div class="col">
                <div>
                <?=nl2br($model_theme->about)?>    
                </div>
                <div>
                    <?php if ($model_theme->getModelGallery()):?>
                    <br>
                    <?php foreach ($model_theme->getModelGallery()->getImgModelList() as $id_img => $model_image): ?>
                    <a  href="<?=$model_image->getImgSrc()?>"
                        data-lightbox="roadtrip">
                        <img src="<?=$model_image->getImgSrc()?>"
                            style="max-width:150px; max-height:150px;"
                            alt="<?=htmlspecialchars($model_theme->name)?> Изображение #<?=$id_img?>" />
                    </a>
                    <?php endforeach;?>
                    <?php endif; ?>
                </div>
            </div>
            
        </div>
    </div>
</div>



<?php foreach ($model_messages_list_info['models'] as $model_message):?>

<div class="card">

    <div class="card-body">
        <div class="row">
            <div class="col-3">
                <?=$model_message->create_date?>    
                <br>
                <?=$model_message->getModelRelated('id_user')->name?>
                <br>
                <br>
                <?=\xtetis\xmessage\Component::getButtonSendMessage($model_theme->id_user)?>
            </div>
            <div class="col" style="overflow-x: auto;">
                <div>    
                    <?=nl2br($model_message->about)?>    
                </div>
                <div>
                    <?php if ($model_message->getModelGallery()):?>
                    <br>
                    <?php foreach ($model_message->getModelGallery()->getImgModelList() as $id_img => $model_image): ?>
                    <a  href="<?=$model_image->getImgSrc()?>"
                        data-lightbox="roadtrip">
                        <img src="<?=$model_image->getImgSrc()?>"
                            style="max-width:150px; max-height:150px;"
                            alt="<?=htmlspecialchars($model_theme->name)?> Сообщение #<?=$model_message->id?> Изображение #<?=$id_img?>" />
                    </a>
                    <?php endforeach;?>
                    <?php endif; ?>
                </div>
            </div>

        </div>

    </div>
</div>
<?php endforeach;?>


<div>
    <?=$pagination?>
</div>

<?php if (\xtetis\xuser\Component::isLoggedIn()): ?>
<div class="card">
    <div class="card-header text-white bg-secondary ">
        Добавить сообщение
    </div>
    <div class="card-body">


    <?=\xtetis\xform\Component::renderOnlyFormStart([
        'url_validate' => $urls['url_validate_add_message'],
        'form_type'    => 'ajax',
    ]);?>

    <input type="hidden" name="id_theme" value="<?=$model_theme->id?>">

    <?=\xtetis\xform\Component::renderField(
        [
            'template'   => 'textarea',
            'attributes' => [
                'label' => 'Сообщение',
                'name'  => 'about',
                'style' => 'min-height: 150px;height: 110px;',

            ],
        ]
    )?>

    <?=\xtetis\xform\Component::renderField(
        [
            'template'   => 'input_image_noaulbum',
            'attributes' => [
                'label'      => 'Прикрепленные изображения',
                'name'       => 'image_list',
                'max_images' => 4,
            ],
        ]
    )?>

    <button type="submit"
            class="btn btn-block btn-primary mb-4">Отправить</button>
    <?=\xtetis\xform\Component::renderFormEnd();?>
    </div>
</div>
<?php endif; ?>


