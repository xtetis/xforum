<?php

    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }

    \xtetis\xengine\App::getApp()->setParam('breadcrumbs', [
        [
            'name' => 'Форум',
            'url'  => $urls['url_forum'],
        ],
        [
            'name' => 'Добавить тему',
        ],
    ]);

    // Устанавливаем Title страницы
    \xtetis\xengine\helpers\SeoHelper::setTitle('Форум - Добавить тему - ' . APP_NAME);
?>


<h3>
Добавить тему
</h3>



<?=\xtetis\xform\Component::renderOnlyFormStart([
    'url_validate' => $urls['url_validate_add_theme'],
    'form_type'    => 'ajax',
]);?>

<?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'select',
        'attributes' => [
            'label'   => 'Группа',
            'name'    => 'id_group',
            'options' => (new \xtetis\xforum\models\GroupModel())->getOptionsAddTheme(),

        ],
        'value'      => $id_group,
    ]
)?>


<?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'input_text',
        'attributes' => [
            'label' => 'Тема',
            'name'  => 'name',
        ],
    ]
)?>
<?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'textarea',
        'attributes' => [
            'label' => 'Сообщение',
            'name'  => 'about',
            'style' => 'min-height: 150px;height: 110px;',

        ],
    ]
)?>



<?=\xtetis\xform\Component::renderField(
        [
            'template'   => 'input_image_noaulbum',
            'attributes' => [
                'label'      => 'Прикрепленные изображения',
                'name'       => 'image_list',
                'max_images' => 4,
            ],
        ]
    )?>

<button type="submit"
        class="btn btn-block btn-primary mb-4">Добавить</button>
<?=\xtetis\xform\Component::renderFormEnd();?>

