<?php

    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }

    $h1 = 'Форум';

    \xtetis\xengine\App::getApp()->setParam('breadcrumbs', [
        [
            'name' => 'Форум',
        ],
    ]);

    // Устанавливаем Title страницы
    \xtetis\xengine\helpers\SeoHelper::setTitle(
        'Форум сексвайф и Hotwife - ' . 
        APP_NAME
    );


    // Устанавливаем Description страницы
    \xtetis\xengine\helpers\SeoHelper::setDescription(
        'Свинг форум, знакомства, общение. Сексвайф и Hotwife. Поиск любовника для замужней женщины.'
    );

    // Устанавливаем H1 страницы
    \xtetis\xengine\helpers\SeoHelper::setPageName(
        $h1
    );

    
?>

<style>
.card-xform {}

.card-xform a.nav-link.active {
    color: #000 !important;
}

.card-xform a.nav-link {
    color: #fff;
}

</style>


<h3>
    <?=$h1?>
</h3>



<?php foreach ($model_group_list as $id => $model_group): ?>
<?php if (!intval($model_group->id_parent)):?>
<div class="card card-xform">
    <div class="card-header text-white bg-secondary ">

        <a href="<?=$model_group->getLink()?>">
            <h4 class=" text-white">
                <?=$model_group->name?>
            </h4>
        </a>

        <ul class="nav nav-tabs card-header-tabs">
            <li class="nav-item">
                <a class="nav-link active "
                   id="last-themes-tab-<?=$id?>"
                   data-toggle="tab"
                   href="#last-themes-<?=$id?>"
                   role="tab"
                   aria-controls="last-themes-<?=$id?>"
                   aria-selected="true">Последние темы</a>
            </li>
            <?php if ($model_group->getChildGroupModelList()):?>
            <li class="nav-item">
                <a class="nav-link  text-white"
                   id="home-tab-<?=$id?>"
                   data-toggle="tab"
                   href="#home-<?=$id?>"
                   role="tab"
                   aria-controls="home-<?=$id?>"
                   aria-selected="false">Дочерние группы</a>
            </li>
            <?php endif; ?>
            <li class="nav-item">
                <a class="nav-link"
                   href="<?=$model_group->getLinkAddTheme()?>">
                   Добавить тему
                   <i class="fa fa-plus" aria-hidden="true"></i>


                </a>
            </li>
            
        </ul>
    </div>
    <div class="card-body">
        <div class="tab-content"
             id="myTabContent">

            <div class="tab-pane fade show active"
                 id="last-themes-<?=$id?>"
                 role="tabpanel"
                 aria-labelledby="last-themes-tab-<?=$id?>">
                 <?=\xtetis\xforum\Component::renderBlock(
                    'blocks/themes_list',
                    [
                        'model_list'=>$model_group->getLatestThemesModelList()
                    ]
                )?>
            </div>
            <?php if ($model_group->getChildGroupModelList()):?>
            <div class="tab-pane fade"
                 id="home-<?=$id?>"
                 role="tabpanel"
                 aria-labelledby="home-tab-<?=$id?>">
                <ul class="list-group list-group-flush">
                <?php foreach($model_group->getChildGroupModelList() as $id_child => $model_group_child):?>
                    <li class="list-group-item">
                        <a href="<?=$model_group_child->getLink()?>">
                            <?=$model_group_child->name?>
                        </a>
                    </li>
                <?php endforeach;?>
                </ul>
            </div>
            <?php endif; ?>
        </div>
    </div>

</div>
<?php endif; ?>
<?php endforeach;?>
