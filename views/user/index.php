<?php

    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }

    \xtetis\xengine\App::getApp()->setParam('breadcrumbs', [
        [
            'name' => 'Форум',
            'url'  => $urls['url_forum'],
        ],
        [
            'name' => 'Пользователи форума',
        ],
    ]);

    // Устанавливаем Title страницы
    \xtetis\xengine\helpers\SeoHelper::setTitle(
        'Список пользователей свинг форума - ' .
        APP_NAME
    );

    // Устанавливаем Description страницы
    \xtetis\xengine\helpers\SeoHelper::setDescription(
        'Пользователи форума для свинг знакомств, смены партнеров или, как говорят - обмена супругами, сексуальными партнерами'
    );


    // Устанавливаем Description страницы
    \xtetis\xengine\helpers\SeoHelper::setPageName(
        'Пользователи форума'
    );
    
?>
<?php foreach ($model_forum_user->model_self_list as $id => $model_forum_user): ?>
<div class="card">
    <div class="card-body">
        <a href="<?=$model_forum_user->getLink();?>"
           class="card-link">
            <h5 class="card-title">
                <?=$model_forum_user->model_user->getUserLoginOrName()?>
            </h5>
        </a>
        <div class="container">
            <div class="row">
                <div class="col">
                    Создано тем: <?=$model_forum_user->getThemesCountTotal()?>
                </div>
                <div class="col">
                    Ответов в темах: <?=$model_forum_user->getMessageCountTotal()?>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <br>
                    Последняя активность <?=$model_forum_user->model_user->getLastAccessFormatted()?>
                </div>
            </div>
        </div>

        </p>
    </div>
</div>
<?php endforeach;?>

<div class="">
    <?=$pagination?>
</div>
