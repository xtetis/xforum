<?php

    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }

    \xtetis\xengine\App::getApp()->setParam('breadcrumbs', [
        [
            'name' => 'Форум',
            'url'  => $urls['url_forum'],
        ],
        [
            'name' => 'Пользователи форума',
            'url'  => $urls['url_forum_users'],
        ],
        [
            'name' => 'Профиль '.$model->model_user->getUserLoginOrName(),
        ],
    ]);


    // Устанавливаем Title страницы
    \xtetis\xengine\helpers\SeoHelper::setTitle(
        'Профиль пользователя форума '.$model->model_user->getUserLoginOrName().' - ' . 
        APP_NAME
    );



    // Устанавливаем Description страницы
    \xtetis\xengine\helpers\SeoHelper::setDescription(
        'Форум - профиль пользователя '.$model->model_user->getUserLoginOrName()
    );
?>
<h1>
    <?=$model->model_user->getUserLoginOrName()?>
</h1>

<nav class="nav nav-pills flex-column flex-sm-row">
    <a class="flex-sm-fill text-sm-center nav-link active"
       href="<?=$urls['url_forum_user_themes']?>">
        Темы
        (<?=$model->getThemesCountTotal()?>)
    </a>
    <a class="flex-sm-fill text-sm-center nav-link"
       href="<?=$urls['url_forum_user_messages']?>"">
        Сообщения
        (<?=$model->getMessageCountTotal()?>)
    </a>
</nav>
