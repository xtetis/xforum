<?php

namespace xtetis\xforum\models;

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

/**
 *
 */
class MessageModel extends \xtetis\xengine\models\TableModel
{

    /**
     * ID темы
     */
    public $id = 0;

    /**
     * @var string
     */
    public $table_name = 'xforum_message';

    

    /**
     * Возвращает связанную модель или false
     * Модели с аттрибутами связываются в массиве related_rule_list, например
     *
     * $this->related_rule_list = [
     *      'attribute_name' => \xtetis\xanyfield\models\ComponentModel::class
     * ];
     *
     * Связанная модель получается методом $this->getModelRelated('attribute_name')
     */
    public $related_rule_list = [
        'id_user'  => \xtetis\xuser\models\UserModel::class,
        'id_theme' => \xtetis\xforum\models\ThemeModel::class,
        'id_gallery' => \xtetis\ximg\models\GalleryModel::class,
    ];

    /**
     * Ссылка на тему
     */
    public $id_theme = 0;

    /**
     * Текст сообщения
     */
    public $about = '';

    /**
     * Массив с каритками в base64 для сообщения
     */
    public $image_list = [];

    /**
     * Помечено ли сообщение как удаленное
     */
    public $deleted = 0;

    /**
     * ID пользователя
     */
    public $id_user = 0;

    /**
     * ID Галереи
     */
    public $id_gallery = 0;

    /**
     * Дата создания записи
     */
    public $create_date = '';

    /**
     * Максимальная длина сообщения
     */
    public $message_max_length = 2000;

    /**
     * Добавляет сообщение в группу
     */
    public function addMessage()
    {

        if ($this->getErrors())
        {
            return false;
        }

        $this->id_theme = intval($this->id_theme);
        $this->about    = strval($this->about);
        $this->id_user  = intval($this->id_user);

        $this->about = strip_tags($this->about);

        if (!\xtetis\xuser\Component::isLoggedIn())
        {
            $this->addError('id_user', 'Только для авторизированных пользователей');

            return false;
        }

        $this->id_user = \xtetis\xuser\Component::isLoggedIn()->id;

        if (!strlen($this->about))
        {
            $this->addError('about', 'Не указано сообщение');

            return false;
        }

        if (mb_strlen($this->about) > $this->message_max_length)
        {
            $this->addError('about', 'Сообщение не может быть больше '.$this->message_max_length.' символов');

            return false;
        }

        if (!$this->id_theme)
        {
            $this->addError('id_theme', 'Не указана тема');

            return false;
        }

        $model_theme = \xtetis\xforum\models\ThemeModel::generateModelById($this->id_theme);
        if (!$model_theme)
        {
            $this->addError('id_theme', 'Тема не существует');

            return false;
        }

        if (!is_array($this->image_list))
        {
            $this->addError('image_list', 'Выберите изображение');

            return false;
        }


        // Проверяем каждую картинку на возможность загрузки
        foreach ($this->image_list as $image_b64)
        {
            $model_upload_image = new \xtetis\ximg\models\ImgUploadModel([
                'filedata' => $image_b64,
            ]);

            // Проверяем параметры файла и возможность загрузки файла
            if (!$model_upload_image->checkUploadFile())
            {
                $this->addError('image_list', $model_upload_image->getLastErrorMessage());

                return false;
            }
        }


        $this->insert_update_field_list = [
            'about',
            'id_theme',
            'id_user',
        ];

        if (!$this->insertTableRecord())
        {
            $this->addError('common', $this->getLastErrorMessage());

            return false;
        }

        if ($this->image_list)
        {
            // Создаем галерею для сообщения
            // ---------------------------------------------
            $model_gallery = new \xtetis\ximg\models\GalleryModel([
                'id_category' => XFORUM_PARENT_GALLERY_CATEGORY,
                'name'        => 'forum_message_gallery_' . $this->id,
            ]);

            if (!$model_gallery->addGallery())
            {
                $this->addError('image_list', $model_gallery->getLastErrorMessage());

                return false;
            }

            // Загружаем изображения обложки
            foreach ($this->image_list as $image_b64)
            {
                $img_model = new \xtetis\ximg\models\ImgModel([
                    'id_gallery' => $model_gallery->id,
                    'filedata'   => $image_b64,
                ]);
                if (!$img_model->uploadImageAndSaveInGallery())
                {
                    $this->addError('image_list', $img_model->getLastErrorMessage());

                    return false;
                }
            }
            // ---------------------------------------------

            // Если все успешно залилось - обновляем в БД id_gallery_cover
            $this->id_gallery = $model_gallery->id;

            // Список полей, которые обновить
            $this->insert_update_field_list = ['id_gallery'];

            // Обновляем данные в таблице
            $this->updateTableRecordById();
        }



        return true;
    }

    /**
     * Редактирует группу
     */
    /*
    public function editGroup()
    {

        if ($this->getErrors())
        {
            return false;
        }

        $this->name      = strval($this->name);
        $this->id        = intval($this->id);
        $this->about     = strval($this->about);
        $this->id_parent = intval($this->id_parent);

        $this->about = strip_tags($this->about);

        if (!strlen($this->name))
        {
            $this->addError('name', 'Не указан name');

            return false;
        }

        if (mb_strlen($this->name) > 199)
        {
            $this->addError('name', 'Имя группы не может быть больше 199 символов');

            return false;
        }

        if (!$this->id)
        {
            $this->addError('id', 'Не указан ID группы');

            return false;
        }

        $model = self::generateModelById($this->id);
        if (!$model)
        {
            $this->addError('id', 'Группа с id=' . $this->id . ' не существует');

            return false;
        }

        if (!strlen($this->about))
        {
            $this->addError('about', 'Не указан about');

            return false;
        }

        if (mb_strlen($this->about) > 400)
        {
            $this->addError('about', 'Описание группы не может быть больше 400 символов');

            return false;
        }

        if ($this->id_parent)
        {
            $model_parent = self::generateModelById($this->id_parent);
            if (!$model_parent)
            {
                $this->addError('id_parent', 'Родительская группа не существует');

                return false;
            }

            // Выбирать в качестве родительского можно только корень или темы первой вложенности
            if (intval($model_parent->id_parent))
            {
                $this->addError('id_parent', 'Выбирать в качестве родительского можно только корень или темы первой вложенности');

                return false;
            }
        }

        if (!$this->id_parent)
        {
            $this->id_parent = null;
        }

        $this->insert_update_field_list = [
            'name',
            'about',
            'id_parent',
        ];
        if (!$this->updateTableRecordById())
        {
            return false;
        }

        return true;
    }
    */

    /**
     * Сначала помечает группу как удаленную, а потом удаляет группу из БД
     */
    /*
    public function deleteGroup()
    {

        if ($this->getErrors())
        {
            return false;
        }

        $this->deleted = intval($this->deleted);

        if (!$this->deleted)
        {
            $this->deleted                  = 1;
            $this->insert_update_field_list = ['deleted'];
            if (!$this->updateTableRecordById())
            {
                return false;
            }

            return true;
        }
        else
        {
            $this->deleteTableRecordById();

            return true;
        }
    }
    */

    /**
     * Возвращает группу из корзины
     */

    /*
    public function undeleteGroup()
    {

        if ($this->getErrors())
        {
            return false;
        }

        $this->deleted                  = 0;
        $this->insert_update_field_list = ['deleted'];
        if (!$this->updateTableRecordById())
        {
            return false;
        }

        return true;

    }
    */



    /**
     * Возвращает модель галереи или FALSE
     */
    public function getModelGallery()
    {

        if ($this->getErrors())
        {
            return false;
        }

        return $this->getModelRelated('id_gallery');

    }
    
    /**
     * Возвращает список тем для выбора в качестве родительской
     * (при редактировании списка тем а вдминке) - возвращает только корень или те,
     * у кого родительская тема - корневая
     */

    /*
    public function getOptionsParent($add_empty = true)
    {
        if ($this->getErrors())
        {
            return false;
        }

        $sql = 'SELECT id FROM xforum_group WHERE COALESCE(id_parent,0) = 0';

        $models = self::getModelListBySql(
            $sql
        );

        $options = [];

        if ($add_empty)
        {
            $options[0] = 'Корень';
        }

        foreach ($models as $id => $model)
        {
            $options[$id] = $model->name;
        }

        return $options;
    }
    */

    /**
     * Возвращает список тем при добавлении темы форума
     */

    /*
    public function getOptionsAddTheme()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $sql = 'SELECT id FROM xforum_group WHERE COALESCE(id_parent,0) = 0';

        $models = self::getModelListBySql(
            $sql
        );

        $options = [];

        foreach ($models as $id => $model)
        {
            $options[$id] = $model->name;
            foreach ($model->getChildGroupModelList() as $id_children => $model_children)
            {
                $options[$id_children] = ' → '.$model_children->name;
            }
        }

        return $options;
    }
    */

    /**
     * Возвращает ссылку на сообщение
     */
    public function getLink()
    {
        return \xtetis\xforum\Component::makeUrl([
            'path'  => [
                'theme',
                $this->id_theme,
            ],
            'query' => [
                'id_message' => $this->id,
            ],
        ]);
    }

    /**
     * Возвращает ссылку на добавление тебы в группу
     */
    /*
    public function getLinkAddTheme()
    {

        return \xtetis\xforum\Component::makeUrl([
            'path'  => [
                'theme',
                'add',
            ],
            'query' => [
                'id_group' => $this->id,
            ],
        ]);
    }
    */

    /**
     * Возвращает список моделей дочерних групп
     */
    /*
    public function getChildGroupModelList()
    {
        if ($this->getErrors())
        {
            return false;
        }

        if (isset($this->children_group_model_list))
        {
            return $this->children_group_model_list;
        }

        $this->id = intval($this->id);

        $sql = 'SELECT id FROM xforum_group WHERE id_parent = :id_parent';

        $this->children_group_model_list = self::getModelListBySql(
            $sql,
            [
                'id_parent'=>$this->id
            ]
        );

        return $this->children_group_model_list;
    }
    */

}
