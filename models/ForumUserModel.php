<?php

namespace xtetis\xforum\models;

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

/**
 *
 */
class ForumUserModel extends \xtetis\xengine\models\Model
{

    /**
     * ID темы
     */
    public $id_user = 0;

    /**
     * Смещение
     */
    public $offset = 20;

    /**
     * Список моделей пользователей по параметрам
     */
    public $list_user_model_params = [];

    /**
     * Список моделей self
     */
    public $model_self_list = [];

    /**
     * Количество пользователей форума без учета пагинации
     */
    public $total_count = [];

    /**
     * Модель пользователя
     */
    public $model_user = false;

    /**
     * Возвращает связанную модель или false
     * Модели с аттрибутами связываются в массиве related_rule_list, например
     *
     * $this->related_rule_list = [
     *      'attribute_name' => \xtetis\xanyfield\models\ComponentModel::class
     * ];
     *
     * Связанная модель получается методом $this->getModelRelated('attribute_name')
     */
    public $related_rule_list = [

        'id_user' => \xtetis\xuser\models\UserModel::class,
    ];

    /**
     * Получаем список моделей пользователей xuser
     */
    public function getUserModelListParams()
    {

        if ($this->getErrors())
        {
            return false;
        }

        $this->offset = intval($this->offset);

        $this->list_user_model_params = \xtetis\xuser\models\UserModel::getListModelsParams([
            'offset' => $this->offset,
            'where'  => [
                'id IN (
                    SELECT DISTINCT id_user
                    FROM
                    (
                        SELECT DISTINCT `id_user` FROM `xforum_theme`
                        UNION
                        SELECT DISTINCT `id_user` FROM `xforum_message`
                    ) tmp
                )',
            ],
        ]);

        return true;
    }

    /**
     * Получаем список моделей пользователей форума
     */
    public function getForumUserModelListParams()
    {

        if ($this->getErrors())
        {
            return false;
        }

        $this->getUserModelListParams();

        $this->model_self_list = [];

        // Количество пользователей форума без учета пагинации
        $this->total_count = $this->list_user_model_params['count'];

        foreach ($this->list_user_model_params['models'] as $id => $model_user)
        {
            $self_model                             = new self();
            $self_model->model_user                 = $model_user;
            $self_model->id_user                    = $model_user->id;
            $this->model_self_list[$model_user->id] = $self_model;
        }

        return true;
    }

    /**
     * Возвращает ссылку на пользоваателя форума
     */
    public function getLink($params = [])
    {

        $with_host = false;
        if (isset($params['with_host']))
        {
            $with_host = $params['with_host'];
        }

        return \xtetis\xforum\Component::makeUrl([
            'path' => [
                'user',
                'profile',
                $this->id_user,
            ],
            'with_host' => $with_host,
        ]);
    }




    /**
     * Возвращает количество тем, созданных пользователем
     */
    public function getThemesCountTotal()
    {

        if ($this->getErrors())
        {
            return false;
        }

        $this->id_user = intval($this->id_user);

        $sql = 'SELECT COUNT(*) count  FROM `xforum_theme` WHERE `id_user` = '.$this->id_user;

        // Получаем количество
        $count = \xtetis\xforum\models\ThemeModel::getCountBySql(
            $sql,
            [], 
            [
                'cache'      => true,
                'cache_tags' => [
                    'xforum_theme',
                ],
            ]
        );

        return $count;
    }




    /**
     * Возвращает количество ответов в темах
     */
    public function getMessageCountTotal()
    {

        if ($this->getErrors())
        {
            return false;
        }

        $this->id_user = intval($this->id_user);

        $sql = 'SELECT COUNT(*) count  FROM `xforum_message` WHERE `id_user` = '.$this->id_user;

        // Получаем количество
        $count = \xtetis\xforum\models\MessageModel::getCountBySql(
            $sql,
            [], 
            [
                'cache'      => true,
                'cache_tags' => [
                    'xforum_message',
                ],
            ]
        );

        return $count;
    }



    /**
     * Возвращает текущую модель пользователя форума
     */
    public function getForumUserModel()
    {

        if ($this->getErrors())
        {
            return false;
        }

        $this->id_user = intval($this->id_user);

        $this->list_user_model_params = \xtetis\xuser\models\UserModel::getListModelsParams([
            'where'  => [
                'id IN (
                    '.$this->id_user.'
                )',
                'id IN (
                    SELECT DISTINCT id_user
                    FROM
                    (
                        SELECT DISTINCT `id_user` FROM `xforum_theme`
                        UNION
                        SELECT DISTINCT `id_user` FROM `xforum_message`
                    ) tmp
                )',
            ],
        ]);


        $this->model_self_list = [];

        // Количество пользователей форума без учета пагинации
        $this->total_count = $this->list_user_model_params['count'];

        foreach ($this->list_user_model_params['models'] as $id => $model_user)
        {
            $self_model                             = new self();
            $self_model->model_user                 = $model_user;
            $self_model->id_user                    = $model_user->id;
            return $self_model;
        }

        return false;
    }




    /**
     * Получаем список моделей тем форума, созданных текущим пользовтелем
     */
    public function getForumUserThemesModelListParams()
    {

        if ($this->getErrors())
        {
            return false;
        }

        $this->getUserModelListParams();

        $this->model_self_list = [];

        // Количество пользователей форума без учета пагинации
        $this->total_count = $this->list_user_model_params['count'];

        foreach ($this->list_user_model_params['models'] as $id => $model_user)
        {
            $self_model                             = new self();
            $self_model->model_user                 = $model_user;
            $self_model->id_user                    = $model_user->id;
            $this->model_self_list[$model_user->id] = $self_model;
        }

        return true;
    }
}
