<?php

namespace xtetis\xforum\models;

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

/**
 *
 */
class ThemeModel extends \xtetis\xengine\models\TableModel
{

    /**
     * ID темы
     */
    public $id = 0;

    /**
     * @var string
     */
    public $table_name = 'xforum_theme';

    /**
     * Возвращает связанную модель или false
     * Модели с аттрибутами связываются в массиве related_rule_list, например
     *
     * $this->related_rule_list = [
     *      'attribute_name' => \xtetis\xanyfield\models\ComponentModel::class
     * ];
     *
     * Связанная модель получается методом $this->getModelRelated('attribute_name')
     */
    public $related_rule_list = [

        'id_user' => \xtetis\xuser\models\UserModel::class,
        'id_gallery' => \xtetis\ximg\models\GalleryModel::class,
    ];

    /**
     * Максимальная длина описания темы
     */
    public $max_langth_about = 8000;

    /**
     * Название темы
     */
    public $name = '';

    /**
     * Описание группы
     */
    public $about = '';

    /**
     * Помечена ли тема как опубликованная
     */
    public $published = 0;

    /**
     * ID Галереи
     */
    public $id_gallery = 0;

    /**
     * Помечена ли тема как удаленная
     */
    public $deleted = 0;

    /**
     * ID группы
     */
    public $id_group = 0;

    /**
     * ID пользователя
     */
    public $id_user = 0;

    /**
     * Дата создания записи
     */
    public $create_date = '';

    /**
     * Массив с каритками в base64 для темы
     */
    public $image_list = [];

    /**
     * Добавляет тему в группу
     */

    public function addTheme()
    {

        if ($this->getErrors())
        {
            return false;
        }

        $this->name     = strval($this->name);
        $this->about    = strval($this->about);
        $this->id_group = intval($this->id_group);

        $this->about = strip_tags($this->about);
        $this->name  = strip_tags($this->name);

        if (!\xtetis\xuser\Component::isLoggedIn())
        {
            $this->addError('id_user', 'Только для авторизированных пользователей');

            return false;
        }

        $this->id_user = \xtetis\xuser\Component::isLoggedIn()->id;

        if (!strlen($this->name))
        {
            $this->addError('name', 'Не указана тема');

            return false;
        }

        if (mb_strlen($this->name) > 199)
        {
            $this->addError('name', 'Имя темы не может быть больше 199 символов');

            return false;
        }

        if (!strlen($this->about))
        {
            $this->addError('about', 'Не указано описание темы');

            return false;
        }

        if (mb_strlen($this->about) > $this->max_langth_about)
        {
            $this->addError('about', 'Описание темы не может быть больше '.$this->max_langth_about.' символов');

            return false;
        }

        if (!$this->id_group)
        {
            $this->addError('id_group', 'Не указана группа');

            return false;
        }

        $model_group = \xtetis\xforum\models\GroupModel::generateModelById($this->id_group);
        if (!$model_group)
        {
            $this->addError('id_group', 'Группа не существует');

            return false;
        }

        if (!is_array($this->image_list))
        {
            $this->addError('image_list', 'Выберите изображение');

            return false;
        }


        // Проверяем каждую картинку на возможность загрузки
        foreach ($this->image_list as $image_b64)
        {
            $model_upload_image = new \xtetis\ximg\models\ImgUploadModel([
                'filedata' => $image_b64,
            ]);

            // Проверяем параметры файла и возможность загрузки файла
            if (!$model_upload_image->checkUploadFile())
            {
                $this->addError('image_list', $model_upload_image->getLastErrorMessage());

                return false;
            }
        }


        $this->insert_update_field_list = [
            'name',
            'about',
            'id_group',
            'id_user',
        ];

        if (!$this->insertTableRecord())
        {
            return false;
        }

        if ($this->image_list)
        {
            // Создаем галерею для сообщения
            // ---------------------------------------------
            $model_gallery = new \xtetis\ximg\models\GalleryModel([
                'id_category' => XFORUM_PARENT_GALLERY_CATEGORY,
                'name'        => 'forum_theme_gallery_' . $this->id,
            ]);

            if (!$model_gallery->addGallery())
            {
                $this->addError('image_list', $model_gallery->getLastErrorMessage());

                return false;
            }

            // Загружаем изображения обложки
            foreach ($this->image_list as $image_b64)
            {
                $img_model = new \xtetis\ximg\models\ImgModel([
                    'id_gallery' => $model_gallery->id,
                    'filedata'   => $image_b64,
                ]);
                if (!$img_model->uploadImageAndSaveInGallery())
                {
                    $this->addError('image_list', $img_model->getLastErrorMessage());

                    return false;
                }
            }
            // ---------------------------------------------

            // Если все успешно залилось - обновляем в БД id_gallery_cover
            $this->id_gallery = $model_gallery->id;

            // Список полей, которые обновить
            $this->insert_update_field_list = ['id_gallery'];

            // Обновляем данные в таблице
            $this->updateTableRecordById();
        }

        return true;
    }

    /**
     * Возвращает ссылку на тему
     */
    public function getLink($params = [])
    {

        $with_host = false;
        if (isset($params['with_host']))
        {
            $with_host = $params['with_host'];
        }

        return \xtetis\xforum\Component::makeUrl([
            'path' => [
                'theme',
                $this->id,
            ],
            'with_host' => $with_host,
        ]);
    }

    /**
     * Возвращает модель галереи или FALSE
     */
    public function getModelGallery()
    {

        if ($this->getErrors())
        {
            return false;
        }

        return $this->getModelRelated('id_gallery');

    }

    /**
     * Возвращает список моделей сообщений постранично для текущей темы
     */
    public function getMessageModelPageListInfo($page = 1)
    {
        if ($this->getErrors())
        {
            return false;
        }

        $model_message_list_info = \xtetis\xforum\models\MessageModel::getListModelsParams([
            'where'  => [
                'COALESCE(deleted,0) = 0',
                'id_theme IN (' . $this->id . ')',
            ],
            'offset' => (20 * ($page - 1)),
        ]);

        return $model_message_list_info;
    }

    /**
     * Возвращает список моделей постранично
     */
    public function getThemesModelPageListInfo($params = [
        'offset' => 0,
    ])
    {
        if ($this->getErrors())
        {
            return false;
        }

        $model_themes_list_info = self::getListModelsParams(
            $params
        );

        return $model_themes_list_info;
    }

    /**
     * Сначала помечает тему как удаленную, а потом удаляет группу из БД
     */
    public function deleteTheme()
    {

        if ($this->getErrors())
        {
            return false;
        }

        $this->deleted = intval($this->deleted);

        if (!$this->deleted)
        {
            $this->deleted                  = 1;
            $this->insert_update_field_list = ['deleted'];
            if (!$this->updateTableRecordById())
            {
                return false;
            }

            return true;
        }
        else
        {
            $this->deleteTableRecordById();

            return true;
        }
    }

    /**
     * Возвращает тему из корзины
     */
    public function undeleteTheme()
    {

        if ($this->getErrors())
        {
            return false;
        }

        $this->deleted                  = 0;
        $this->insert_update_field_list = ['deleted'];
        if (!$this->updateTableRecordById())
        {
            return false;
        }

        return true;

    }

    /**
     * Изменяет публикацию темы
     */
    public function setPublishTheme($pub = 0)
    {

        if ($this->getErrors())
        {
            return false;
        }

        $pub = intval($pub);
        if ($pub)
        {
            $pub = 1;
        }

        $this->published                = $pub;
        $this->insert_update_field_list = ['published'];
        if (!$this->updateTableRecordById())
        {
            return false;
        }

        return true;

    }

    /**
     * Редактирует тему
     */
    public function editTheme()
    {

        if ($this->getErrors())
        {
            return false;
        }

        $this->name     = strval($this->name);
        $this->id       = intval($this->id);
        $this->about    = strval($this->about);
        $this->id_group = intval($this->id_group);

        $this->about = strip_tags($this->about);

        if (!strlen($this->name))
        {
            $this->addError('name', 'Не указан name');

            return false;
        }

        if (mb_strlen($this->name) > 199)
        {
            $this->addError('name', 'Имя группы не может быть больше 199 символов');

            return false;
        }

        if (!$this->id)
        {
            $this->addError('id', 'Не указан ID темы');

            return false;
        }

        $model = self::generateModelById($this->id);
        if (!$model)
        {
            $this->addError('id', 'Тема с id=' . $this->id . ' не существует');

            return false;
        }

        if (!strlen($this->about))
        {
            $this->addError('about', 'Не указан about');

            return false;
        }

        if (mb_strlen($this->about) > $this->max_langth_about)
        {
            $this->addError('about', 'Описание темы не может быть больше '.$this->max_langth_about.' символов');

            return false;
        }

        $model_group = \xtetis\xforum\models\GroupModel::generateModelById($this->id_group);
        if (!$model_group)
        {
            $this->addError('id_group', 'Родительская группа не существует');

            return false;
        }

        $this->insert_update_field_list = [
            'name',
            'about',
            'id_group',
        ];
        if (!$this->updateTableRecordById())
        {
            return false;
        }

        return true;
    }


    /**
     * Возвращает количество сообщений для текущей темы
     */
    public function getMessageCount()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $sql = "
        SELECT count(*) count 
        FROM `xforum_message` 
        WHERE 
            `id_theme` = :id_theme
            AND
            COALESCE(`deleted`,0) = 0 
        ";
        return $this->getCountBySql(
            $sql,
            [
                'id_theme'=>intval($this->id)
            ],
            [
                'cache'      => true,
                'cache_tags' => [
                    'xforum_message'
                ],
            ]
        );
    }

}
