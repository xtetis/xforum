<?php

namespace xtetis\xforum\models;

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

/**
 *
 */
class GroupModel extends \xtetis\xengine\models\TableModel
{

    /**
     * ID статьи
     */
    public $id = 0;

    /**
     * @var string
     */
    public $table_name = 'xforum_group';

    /**
     * Возвращает связанную модель или false
     * Модели с аттрибутами связываются в массиве related_rule_list, например
     *
     * $this->related_rule_list = [
     *      'attribute_name' => \xtetis\xanyfield\models\ComponentModel::class
     * ];
     *
     * Связанная модель получается методом $this->getModelRelated('attribute_name')
     */
    public $related_rule_list = [
        'id_parent' => \xtetis\xforum\models\GroupModel::class,
    ];

    /**
     * Изображение (используется при добавлении темы)
     */
    public $image = [];

    /**
     * Название группы
     */
    public $name = '';

    /**
     * Родительская тема (поддерживается 2-х уровневавая вложенность)
     * Родительской темой может быть корено или тема, у которой родительская тема = корень
     */
    public $id_parent = 0;

    /**
     * Описание группы
     */
    public $about = '';

    /**
     * ПРивязанная галерея (единственное изображение лого темы)
     */
    public $id_gallery = 0;

    /**
     * Помечена ли группа как удаленная
     */
    public $deleted = 0;

    /**
     * Дата создания записи
     */
    public $create_date = '';

    /**
     * Список моделей дочерних групп
     */
    public $children_group_model_list;

    /**
     * ВОзвращает модели всех групп
     */
    public static function getAllGroupModels()
    {
        $sql = 'SELECT id FROM xforum_group';

        return self::getModelListBySql(
            $sql
        );
    }

    /**
     * Добавляет группу
     */
    public function addGroup()
    {

        if ($this->getErrors())
        {
            return false;
        }

        $this->name      = strval($this->name);
        $this->about     = strval($this->about);
        $this->id_parent = intval($this->id_parent);

        $this->about = strip_tags($this->about);
        $this->name  = strip_tags($this->name);

        if (!strlen($this->name))
        {
            $this->addError('name', 'Не указан name');

            return false;
        }

        if (mb_strlen($this->name) > 199)
        {
            $this->addError('name', 'Имя группы не может быть больше 199 символов');

            return false;
        }

        if (!strlen($this->about))
        {
            $this->addError('about', 'Не указан about');

            return false;
        }

        if (mb_strlen($this->about) > 400)
        {
            $this->addError('about', 'Описание группы не может быть больше 400 символов');

            return false;
        }

        if ($this->id_parent)
        {
            $model_parent = self::generateModelById($this->id_parent);
            if (!$model_parent)
            {
                $this->addError('id_parent', 'Родительская группа не существует');

                return false;
            }

            // Выбирать в качестве родительского можно только корень или темы первой вложенности
            if (intval($model_parent->id_parent))
            {
                $this->addError('id_parent', 'Выбирать в качестве родительского можно только корень или темы первой вложенности');

                return false;
            }
        }

        if (!$this->id_parent)
        {
            $this->id_parent = null;
        }

        if (!is_array($this->image))
        {
            $this->addError('image', 'Выберите изображение в качестве обложки темы');

            return false;
        }

        if (count($this->image) > 1)
        {
            $this->addError('image', 'Изображение для обложки вечеринки должно быть только одно');

            return false;
        }

        // Проверяем каждую картинку на возможность загрузки
        foreach ($this->image as $image_cover_b64)
        {
            $model_upload_image = new \xtetis\ximg\models\ImgUploadModel([
                'filedata' => $image_cover_b64,
            ]);

            // Проверяем параметры файла и возможность загрузки файла
            if (!$model_upload_image->checkUploadFile())
            {
                $this->addError('image', $model_upload_image->getLastErrorMessage());

                return false;
            }
        }

        $this->insert_update_field_list = [
            'name',
            'about',
            'id_parent',
        ];

        if (!$this->insertTableRecord())
        {
            return false;
        }

        return true;
    }

    /**
     * Редактирует группу
     */
    public function editGroup()
    {

        if ($this->getErrors())
        {
            return false;
        }

        $this->name      = strval($this->name);
        $this->id        = intval($this->id);
        $this->about     = strval($this->about);
        $this->id_parent = intval($this->id_parent);

        $this->about = strip_tags($this->about);

        if (!strlen($this->name))
        {
            $this->addError('name', 'Не указан name');

            return false;
        }

        if (mb_strlen($this->name) > 199)
        {
            $this->addError('name', 'Имя группы не может быть больше 199 символов');

            return false;
        }

        if (!$this->id)
        {
            $this->addError('id', 'Не указан ID группы');

            return false;
        }

        $model = self::generateModelById($this->id);
        if (!$model)
        {
            $this->addError('id', 'Группа с id=' . $this->id . ' не существует');

            return false;
        }

        if (!strlen($this->about))
        {
            $this->addError('about', 'Не указан about');

            return false;
        }

        if (mb_strlen($this->about) > 400)
        {
            $this->addError('about', 'Описание группы не может быть больше 400 символов');

            return false;
        }

        if ($this->id_parent)
        {
            $model_parent = self::generateModelById($this->id_parent);
            if (!$model_parent)
            {
                $this->addError('id_parent', 'Родительская группа не существует');

                return false;
            }

            // Выбирать в качестве родительского можно только корень или темы первой вложенности
            if (intval($model_parent->id_parent))
            {
                $this->addError('id_parent', 'Выбирать в качестве родительского можно только корень или темы первой вложенности');

                return false;
            }
        }

        if (!$this->id_parent)
        {
            $this->id_parent = null;
        }

        $this->insert_update_field_list = [
            'name',
            'about',
            'id_parent',
        ];
        if (!$this->updateTableRecordById())
        {
            return false;
        }

        return true;
    }

    /**
     * Сначала помечает группу как удаленную, а потом удаляет группу из БД
     */
    public function deleteGroup()
    {

        if ($this->getErrors())
        {
            return false;
        }

        $this->deleted = intval($this->deleted);

        if (!$this->deleted)
        {
            $this->deleted                  = 1;
            $this->insert_update_field_list = ['deleted'];
            if (!$this->updateTableRecordById())
            {
                return false;
            }

            return true;
        }
        else
        {
            $this->deleteTableRecordById();

            return true;
        }
    }

    /**
     * Возвращает группу из корзины
     */
    public function undeleteGroup()
    {

        if ($this->getErrors())
        {
            return false;
        }

        $this->deleted                  = 0;
        $this->insert_update_field_list = ['deleted'];
        if (!$this->updateTableRecordById())
        {
            return false;
        }

        return true;

    }

    /**
     * Возвращает список тем для выбора в качестве родительской
     * (при редактировании списка тем а вдминке) - возвращает только корень или те,
     * у кого родительская тема - корневая
     */
    public function getOptionsParent($add_empty = true)
    {
        if ($this->getErrors())
        {
            return false;
        }

        $sql = 'SELECT id FROM xforum_group WHERE COALESCE(id_parent,0) = 0';

        $models = self::getModelListBySql(
            $sql
        );

        $options = [];

        if ($add_empty)
        {
            $options[0] = 'Корень';
        }

        foreach ($models as $id => $model)
        {
            $options[$id] = $model->name;
        }

        return $options;
    }

    /**
     * Возвращает список тем при добавлении темы форума
     */
    public function getOptionsAddTheme()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $sql = 'SELECT id FROM xforum_group WHERE COALESCE(id_parent,0) = 0';

        $models = self::getModelListBySql(
            $sql
        );

        $options = [];

        foreach ($models as $id => $model)
        {
            $options[$id] = $model->name;
            foreach ($model->getChildGroupModelList() as $id_children => $model_children)
            {
                $options[$id_children] = ' → ' . $model_children->name;
            }
        }

        return $options;
    }

    /**
     * Возвращает ссылку на группу
     */
    public function getLink($params = [])
    {
        $with_host = false;
        if (isset($params['with_host']))
        {
            $with_host = $params['with_host'];
        }

        return \xtetis\xforum\Component::makeUrl([
            'path'  => [
                'group',
                $this->id,
            ],
            'with_host' => $with_host,
        ]);
    }

    /**
     * Возвращает ссылку на добавление тебы в группу
     */
    public function getLinkAddTheme()
    {

        return \xtetis\xforum\Component::makeUrl([
            'path'  => [
                'theme',
                'add',
            ],
            'query' => [
                'id_group' => $this->id,
            ],
        ]);
    }

    /**
     * Возвращает список моделей дочерних групп
     */
    public function getChildGroupModelList()
    {
        if ($this->getErrors())
        {
            return false;
        }

        if (isset($this->children_group_model_list))
        {
            return $this->children_group_model_list;
        }

        $this->id = intval($this->id);

        $sql = 'SELECT id FROM xforum_group WHERE id_parent = :id_parent';

        $this->children_group_model_list = self::getModelListBySql(
            $sql,
            [
                'id_parent' => $this->id,
            ]
        );

        return $this->children_group_model_list;
    }

    /**
     * Возвращает список моделей последних тем (Включая дочерние)
     */
    public function getLatestThemesModelList($count = 5)
    {
        if ($this->getErrors())
        {
            return false;
        }

        $count = intval($count);
        if ($count<1)
        {
            $count = 10;
        }

        if ($count>10)
        {
            $count = 10;
        }

        $all_groups = [$this->id];
        foreach ($this->getChildGroupModelList() as $id => $model)
        {
            $all_groups[] = $id;
        }

        $sql = 'SELECT id FROM xforum_theme WHERE
        published = 1 AND
        COALESCE(deleted,0) = 0 AND
        id_group IN (' . implode(',', $all_groups) . ') ORDER BY id DESC
        LIMIT '.$count.'
        ';

        $models = \xtetis\xforum\models\ThemeModel::getModelListBySql(
            $sql,
            [],
            [
                'cache'=>true,
                'cache_tags'=>[
                    'xforum_theme',
                ]
            ]
        );

        return $models;
    }

    /**
     * Возвращает список моделей постранично
     */
    public function getGroupThemesModelPageListInfo($params = [
        'offset' => 0,
    ])
    {
        if ($this->getErrors())
        {
            return false;
        }

        $all_groups = [$this->id];
        foreach ($this->getChildGroupModelList() as $id => $model)
        {
            $all_groups[] = $id;
        }

        $model_themes_list_info = \xtetis\xforum\models\ThemeModel::getListModelsParams([
            'where'  => [
                'published = 1',
                'COALESCE(deleted,0) = 0',
                'id_group IN (' . implode(',', $all_groups) . ')',
            ],
            'offset' => $params['offset'],
            'order'  => 'id DESC',
        ]);

        return $model_themes_list_info;
    }

    /**
     * Возвращает модель родительской группы
     */
    public function getParentGroupModel()
    {
        if ($this->getErrors())
        {
            return false;
        }

        return $this->getModelRelated('id_parent');
    }

}
