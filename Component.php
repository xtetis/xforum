<?php

namespace xtetis\xforum;

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

class Component extends \xtetis\xengine\models\Component
{

    /**
     * @param array $params
     */
    public function __construct($params = [])
    {
        // Проверяет параметры
        \xtetis\xforum\Config::validateParams();

        parent::__construct($params);
    }

    /**
     * Возвращает массив ссылок для карты сайта
     */
    public static function getSitemapUrlList(): array {
        $ret = [

        ];

        $ret[self::makeUrl(['with_host' => true])] = 'Форум';

        $sql        = 'SELECT id FROM `xforum_theme` WHERE COALESCE(`published`,0) != 0 AND COALESCE(`deleted`,0) = 0';
        $model_list = \xtetis\xforum\models\ThemeModel::getModelListBySql($sql);
        foreach ($model_list as $model_theme)
        {

            $link       = $model_theme->getLink(['with_host' => true]);
            $anchor     = 'Тема: '.$model_theme->name;
            $ret[$link] = $anchor;
        }

        $sql        = 'SELECT * FROM `xforum_group` WHERE COALESCE(`deleted`,0) = 0';
        $model_list = \xtetis\xforum\models\GroupModel::getModelListBySql($sql);
        foreach ($model_list as $model_group)
        {
            $link       = $model_group->getLink(['with_host' => true]);
            $anchor     = 'Группа форума: '.$model_group->name;
            $ret[$link] = $anchor;
        }

        return $ret;
    }

    /**
     * Возвращает блок с последними анкетами
     */
    public static function getBlockLatestThemes($limit = 6)
    {

        // Получаем список моделей тем
        //*****************************************************
        $model_themes_list_info = \xtetis\xforum\models\ThemeModel::getListModelsParams([
            'limit' => intval($limit),
            'order' => 'id DESC',
            'where' => [
                'published = 1',
                'COALESCE(deleted,0) = 0',
            ],
        ]);
        //*****************************************************

        // Рендерим блок с последними темами

        return self::renderBlock('blocks/latest_themes',
            [
                'model_themes_list_info' => $model_themes_list_info,
            ]);

    }

    /**
     * Список роутов для компонента (не APP)
     * Для APP роуты берутся из /engine/config/params.routes.php
     * В самих компонентах роуты можно переназначать
     */
    public static function getComponentRoutes()
    {

        $routes = [
            // https://lsfinder.com/forum/theme/item/15
            [
                'url'      => '/^\/' . self::getComponentUrl() . '\/theme\/item\/(\d+)$/',
                'function' => function (
                    $params = []
                )
                {
                    $id = isset($params[0]) ? $params[0] : '';
                    $id = intval($id);

                    $model = \xtetis\xforum\models\ThemeModel::generateModelById($id);
                    if ($model)
                    {
                        header('HTTP/1.1 301 Moved Permanently');
                        header('Location: ' . $model->getLink());
                        exit();

                    }
                    else
                    {
                        header('HTTP/1.1 301 Moved Permanently');
                        header('Location: /' . self::getComponentUrl());
                        exit();
                    }
                },
            ],

            // https://lsfinder.com/forum/theme/index/15
            [
                'url'      => '/^\/' . self::getComponentUrl() . '\/theme\/index\/(\d+)$/',
                'function' => function (
                    $params = []
                )
                {
                    $id = isset($params[0]) ? $params[0] : '';
                    $id = intval($id);

                    $model = \xtetis\xforum\models\ThemeModel::generateModelById($id);
                    if ($model)
                    {
                        header('HTTP/1.1 301 Moved Permanently');
                        header('Location: ' . $model->getLink());
                        exit();

                    }
                    else
                    {
                        header('HTTP/1.1 301 Moved Permanently');
                        header('Location: /' . self::getComponentUrl());
                        exit();
                    }
                },
            ],

            // https://lsfinder.com/forum/group/item/15
            [
                'url'      => '/^\/' . self::getComponentUrl() . '\/group\/item\/(\d+)$/',
                'function' => function (
                    $params = []
                )
                {
                    $id = isset($params[0]) ? $params[0] : '';
                    $id = intval($id);

                    $model = \xtetis\xforum\models\GroupModel::generateModelById($id);
                    if ($model)
                    {
                        header('HTTP/1.1 301 Moved Permanently');
                        header('Location: ' . $model->getLink());
                        exit();

                    }
                    else
                    {
                        header('HTTP/1.1 301 Moved Permanently');
                        header('Location: /' . self::getComponentUrl());
                        exit();
                    }
                },
            ],

            [
                'url'      => '/^\/(' . self::getComponentUrl() . ')(\/(\w+)?(\/(\d+)?)?)?$/',
                'function' => function (
                    $params = []
                )
                {
                    \xtetis\xengine\App::getApp()->component_name = isset($params[0]) ? $params[0] : '';
                    \xtetis\xengine\App::getApp()->action_name    = isset($params[2]) ? $params[2] : '';
                    \xtetis\xengine\App::getApp()->query_name     = 'index';

                    $id = isset($params[4]) ? $params[4] : '';

                    if (strlen($id))
                    {
                        $_GET['id'] = $id;
                    }
                },
            ],
            [
                'url'      => '/^\/(' . self::getComponentUrl() . ')(\/(\w+)?(\/(\w+)?(\/(\d+)?)?)?)?$/',
                'function' => function (
                    $params = []
                )
                {
                    \xtetis\xengine\App::getApp()->component_name = isset($params[0]) ? $params[0] : '';
                    \xtetis\xengine\App::getApp()->action_name    = isset($params[2]) ? $params[2] : '';
                    \xtetis\xengine\App::getApp()->query_name     = isset($params[4]) ? $params[4] : '';

                    $id = isset($params[6]) ? $params[6] : '';

                    if (strlen($id))
                    {
                        $_GET['id'] = $id;
                    }
                },
            ],
        ];

        return $routes;
    }
}
